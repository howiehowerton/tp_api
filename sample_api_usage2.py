from sms_5_2_0_api import Client
from datetime import datetime
import time, os, shutil, sqlite3

'''
SMS API key that's used to authenticate.  You can retrieve this value from SMS by going to:
Admin > Authentication and Authorization > Users.  
Double click on the desired User account and copy the value of the API key.
'''
sms_api_key = '1BB93A49-A877-4810-B14C-FB8E070347CC'
# SMS's IP can be found in Admin > Server Properties > Network (tab)
sms_server = '192.168.50.10'
# Whether or not to enforce validation of certificates.  Set to False if using the default self-signed certificate on SMS.
verify_certs=False
# Instantiate an instance of the sms_5_api Client 
client = Client(sms_api_key, sms_server, verify_certs=verify_certs)
# Gobal variables that'll make working with sqlite3 easier
global my_conn
global my_cursor


def get_epoch_from_datetime(dt):
    '''Calculate epoch time from a datatime object'''
    epoch_format = str(int(time.mktime(dt.timetuple())))
    return epoch_format


def create_SQLiteDB(db_column_names, output_dir_path, filter_sql, timestamp):
    sqlite_file = str(timestamp) + 'db.sqlite'
    sqlite_file = sqlite_file.replace(" ", "_")
    sqlite_file_path = os.path.join(output_dir_path, sqlite_file)
    if os.path.exists(sqlite_file_path):
        os.remove(sqlite_file_path)
    my_conn = sqlite3.connect(sqlite_file_path)
    my_conn.text_factory = str
    my_cursor = my_conn.cursor()
    my_cursor.execute("CREATE TABLE SIGNATURE (" + db_column_names + ");")  
    my_conn.commit()
    for i in filter_sql:
        my_cursor.execute(i)
    my_conn.commit()
    return my_conn, my_cursor


def get_filters_by_cve(my_cursor, cve):
    my_cursor.execute('select distinct(NUM) from SIGNATURE where CVE_ID like "%{cve}%";'.format(cve=cve))
    results = my_cursor.fetchall()
    filters = []
    for r in results:
        filters.append(r[0])
    return filters


def main():
    '''main routine'''
    epoch_dt = get_epoch_from_datetime(datetime.now())
    # Get the filter data from SMS (which we'll clean up and convert to a local SQLite3 DB)
    try:
        print("Fetching filter data from the SMS API...")
        print("This will take a few seconds..")
        response = client.get_data_dictionary(table='SIGNATURE')
        #print(response)
    except Exception as e:
        print("Error calling API.  The raw error message is:\n\n {e}".format(e=e))
    # Split the data up into individual lines (of SQL Statemetns)
    filter_sql = response.text.split(';\n')
    # Clean up the first and last items of the list
    temp = filter_sql[0]
    filter_sql[0] = temp.replace('-- SIGNATURE\n', '') # Removing test from the first item (due to a sloppy split)
    filter_sql = filter_sql[:-1] #last item contains ''
    # Create the SQLite3 DB
    db_column_names = 'ID,NUM,SEVERITY_ID,NAME,CLASS,PRODUCT_CATEGORY_ID,PROTOCOL,TAXONOMY_ID,CVE_ID,BUGTRAQ_ID,DESCRIPTION,MESSAGE'
    output_dir_path = os.getcwd()
    my_conn, my_cursor = create_SQLiteDB(db_column_names, output_dir_path, filter_sql, timestamp=epoch_dt)
    # Just a quick, dirty lookup of a static CVE
    cve = '2004-0790'
    filters = get_filters_by_cve(my_cursor, cve)
    filter_string = ", ".join(str(filter) for filter in filters)
    print("Filters matching {cve}:  {filters}".format(cve=cve, filters=filter_string))


if __name__ == '__main__':
    main()



'''
cve = '2004-11100'
filters = get_filters_by_cve(my_cursor, cve)
filter_string = ", ".join(str(filter) for filter in filters)
print("Filters matching {cve}:  {filters}".format(cve=cve, filters=filter_string))'''